package com.brst.barcodescanner;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.zxing.client.android.CaptureActivity;
import com.google.zxing.client.android.PreferencesActivity_;
import com.google.zxing.client.android.history.HistoryActivity;
import com.google.zxing.client.android.history.HistoryManager;

public class FirstActivity extends Activity {
	static final String ACTION_SCAN = "com.google.zxing.client.android.SCAN";
	public static final int HISTORY_REQUEST_CODE = 0x0000bacc;
	private HistoryManager historyManager;
	boolean autoScan;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater menuInflater = getMenuInflater();
		menuInflater.inflate(R.menu.capture, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
		int itemId = item.getItemId();
		if (itemId == R.id.menu_history) {
			intent.setClassName(this, HistoryActivity.class.getName());
			startActivity(intent);

		} else if (itemId == R.id.menu_settings) {
			intent.setClassName(this, PreferencesActivity_.class.getName());
			startActivity(intent);
			// Toast.makeText(getApplicationContext(), "To be continued",
			// Toast.LENGTH_SHORT).show();

		} else {
			return super.onOptionsItemSelected(item);
		}
		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 0) {
			if (resultCode == RESULT_OK) {
				String contents = data.getStringExtra("SCAN_RESULT");
				Log.e("", contents);
				SharedPreferences prefs = PreferenceManager
						.getDefaultSharedPreferences(this);
				autoScan = prefs.getBoolean(PreferencesActivity_.AUTO_SCAN,
						false);
				if (autoScan) {
					Toast.makeText(getApplicationContext(),
							"Checked Auto scanned", Toast.LENGTH_SHORT).show();
				} else {
					Toast.makeText(getApplicationContext(),
							"UnChecked Auto scanned", Toast.LENGTH_SHORT)
							.show();
				}

				// if (requestCode == HISTORY_REQUEST_CODE) {
				// int itemNumber = data.getIntExtra(
				// Intents.History.ITEM_NUMBER, -1);
				// if (itemNumber >= 0) {
				// HistoryItem historyItem = historyManager
				// .buildHistoryItem(itemNumber);
				// Log.e("first ", "" + historyItem);
				// Log.e("result :", "" + historyItem.getResult());
				// // CaptureActivity.decodeOrStoreSavedBitmap(null,
				// // historyItem.getResult());
				//
				// // decodeOrStoreSavedBitmap(null,
				// // historyItem.getResult());
				// }
				Intent i = new Intent(this, FirstActivity.class);
				i.putExtra("Content", contents);
				startActivity(i);
				overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
				finish();
				// }
				// Toast.makeText(this, "Barcode:" + contents,
				// Toast.LENGTH_SHORT)
				// .show();

			} else if (resultCode == RESULT_CANCELED) {
				// Handle cancel
				// Toast.makeText(getApplicationContext(), "Failed to Scan",
				// Toast.LENGTH_SHORT).show();
				onBackPressed();

			}
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		historyManager = new HistoryManager(this);
		historyManager.trimHistory();

		Intent intent = new Intent(getApplicationContext(),
				CaptureActivity.class);
		intent.setAction("com.google.zxing.client.android.SCAN");
		// this stops saving ur barcode in barcode scanner app's history
		intent.putExtra("SAVE_HISTORY", true);
		startActivityForResult(intent, 0);

	}

}
